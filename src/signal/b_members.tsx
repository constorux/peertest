import { CtrlBit, WorkerControl } from "../bit/ctrl_bit";
import { AppModel, AppService } from "../service/s_app";
import { AuthService, AuthState } from "../service/s_auth";
import { MemberService, ReadMembership } from "../service/s_member";

type Inputs = { appId: string };
type Data = ReadMembership[];

class Ctrl extends WorkerControl<Inputs, Data> {
  async worker() {
    return MemberService.i.getForApp(this.p.appId);
  }

  private async _setStatus(memberId: string, status: String) {
    await MemberService.i.set(memberId, { status: status });
    //await new Promise((resolve) => setTimeout(resolve, 100));
    this.reload();
  }

  public async approve(userId) {
    this._setStatus(userId, "active");
  }

  public async remove(userId) {
    this._setStatus(userId, "removed");
  }
}

export const AppMembersBit = CtrlBit<Inputs, Data, Ctrl>(
  (p, b) => new Ctrl(p, b),
  "AppMembers"
);
