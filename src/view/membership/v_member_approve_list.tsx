import { showConfirmDialog } from "elbe-ui";
import { Archive, Check, Rocket, Trash } from "lucide-react";
import { route } from "preact-router";
import { AppService } from "../../service/s_app";
import { ReadMembership } from "../../service/s_member";
import { AppMembersBit } from "../../signal/b_members";
import { go, showDate } from "../../util";
import { UserChip } from "../account/v_account_chip";

function TesterView({ member }: { member: ReadMembership }) {
  return (
    <div class="row flex-1 gap">
      <div
        class="tooltipped pointer column cross-stretch gap-half"
        onClick={go("/account/" + member.account)}
      >
        <div class="tooltip">
          <UserChip user={member.expand.account} />
        </div>
        <div class="b">{member.email}</div>

        <div class="">{member.device}</div>
      </div>
      <div class="flex-1 text-s" style="text-align: end">
        since. {showDate(member.updated)}
      </div>
    </div>
  );
}

function _emptyList(list, onEmpty) {
  if (list.length == 0) {
    return onEmpty;
  }
  return list;
}

export function MemberApproveList({ appId }: { appId: string }) {
  return (
    <AppMembersBit.Provide appId={appId}>
      <div class="column cross-stretch gap-double">
        <hr style="color: #00000033; width: 100%; height: 1px" />
        <h3>Manage Legacy Testers</h3>
        <_MemberApproveList appId={appId} />
      </div>
    </AppMembersBit.Provide>
  );
}

export function _MemberApproveList(p: { appId: string }) {
  const { map, ctrl } = AppMembersBit.use();

  /*if (!members || !members.map) {
    return <div class="i">could not load testing management</div>;
  }*/
  return map({
    onError: () => <div class="i">could not load testing management</div>,
    onData: (members) =>
      !members || !members.map ? (
        <div class="i">could not load testing management</div>
      ) : (
        <div class="column cross-stretch gap-double">
          <h5 class="margin-none">requests</h5>
          <div class="column cross-stretch">
            {_emptyList(
              members
                .filter((m) => m.status == "requested")
                .map((m) => (
                  <div class="card row main-space-between">
                    <TesterView member={m} />

                    <div class="row main-end">
                      <button class="action" onClick={() => ctrl.remove(m.id)}>
                        deny
                      </button>
                      <button
                        class="loud minor"
                        onClick={async () => {
                          const s = await showConfirmDialog({
                            okay: true,
                            title: "add in console",
                            message:
                              "make sure to also add the email-address to your <b>closed testers list</b> in the Google Play Console",
                          });
                          ctrl.approve(m.id);
                        }}
                      >
                        <Check />
                        accept
                      </button>
                    </div>
                  </div>
                )),
              "no requests yet"
            )}
          </div>
          <h5 class="margin-none" style="margin-top: 1rem;">
            active testers
          </h5>
          <div class="column cross-stretch">
            {_emptyList(
              members
                .filter((m) => m.status == "active")
                .map((m) => (
                  <div class="card row main-space-between">
                    <TesterView member={m} />
                    <div class="row main-end">
                      <button
                        class="integrated"
                        onClick={() => ctrl.remove(m.id)}
                      >
                        <Trash />
                        remove
                      </button>
                    </div>
                  </div>
                )),
              "no active testers yet"
            )}
          </div>
          <div class="row main-end">
            <button
              class="loud minor"
              onClick={async () => {
                const s = await showConfirmDialog({
                  title: "really finish testing?",
                  message:
                    'all testers that are still present will be rewarded 1 sparkle.<br><b class="b">You can NOT undo this action.</b><br><br><span style="font-size: 0.8rem">This is only possible <b class="b"> after at least 2 weeks</b><br>(to avoid spam).</span>',
                });
                if (!s) return;
                await AppService.i.finishTesting(p.appId);
                route("/account/");
              }}
            >
              <Rocket /> complete testing
            </button>
          </div>
        </div>
      ),
  });
}

export function ArchiveBtn(a: { appId: string }) {
  return !a.appId ? null : (
    <button
      class="integrated"
      onClick={async () => {
        const s = await showConfirmDialog({
          title: "permanently delete?",
          message:
            'This will delete the app and all its data. No Sparkles will be rewarded.<br><b class="b">You can NOT undo this action.</b>',
        });
        if (!s) return;
        await AppService.i.archive(a.appId);
        route("/account/");
      }}
    >
      <Archive /> delete
    </button>
  );
}
