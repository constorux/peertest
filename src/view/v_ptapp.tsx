import { Button, Card, ElbeColorManners } from "elbe-ui";
import { CheckIcon, SmartphoneIcon } from "lucide-react";
import { moewe } from "moewe";

export function AppInPTApp() {
  return (
    <Card class="row cross-stretch accent minor borderless">
      <CheckIcon />
      <div class="column flex-1 gap">
        <div class="b">available on the peerTest app</div>
        <_downloadBtn />
      </div>
    </Card>
  );
}

function _downloadBtn({
  manner = "major",
  label = "Download App",
}: {
  manner?: ElbeColorManners;
  label?: string;
}) {
  return (
    <Button
      manner={manner ?? "major"}
      label={label}
      onTap={() =>
        // open in same tab
        window.open(
          "https://github.com/RobinNaumann/peertest_app/releases/latest/download/peerTest.apk",
          "_self"
        )
      }
    />
  );
}

export function AppDownloadView() {
  return (
    <Card class="row cross-stretch accent minor">
      <SmartphoneIcon />
      <div class="column flex-1 gap-half">
        <div class="b">peerTest App</div>
        <div style="margin-bottom: 0.5rem">
          Download the Android app to test peoples apps
        </div>
        <_downloadBtn />
      </div>
    </Card>
  );
}

export function AppUpdateView() {
  return (
    moewe().config.flagBool("show_app_update") && (
      <Card class="row cross-stretch accent minor">
        <SmartphoneIcon />
        <div class="column flex-1 gap-half">
          <div class="b body-l">important update</div>
          <div style="margin-bottom: 0.5rem">
            Over the past couple of months, it became clear, that many apps fail
            to get past private testing. This is not because Testers are not
            willing to test, but because Google Play is quite strict when it
            comes to the timespans between tests.
            <br />
            Because of this, I've decided to write a <b>peerTest mobile app</b>.
            This streamlines the testing process and makes it easier for you to
            get your app tested. Also, testers are now guaranteed their rewarded
            after a fixed number of tests. (even if the app is not published).
            To highlight this change, I've decided to replace Sparkles with
            Beakers.
            <br />
            <br />
            <b>What does this mean for you? </b>
            You can still test apps on the website, but I highly recommend you
            check out the app. It's available below.
            <br />
            <br />
            Thank you for being a part of this community.
            <br /> <b>Yours, Robin</b>
            <br />
            <br />
            <div class="body-s">
              If you'd like to support me, consider{" "}
              <a
                style="color: black"
                href="https://www.buymeacoffee.com/robinnaumann"
              >
                donating
              </a>
              . It's greatly appreciated.
            </div>
          </div>
          <_downloadBtn label="Download peerTest App" />
          <div class="body-s">
            (Google Play does not allow apps that manage other apps, so you have
            to download the .apk from here)
          </div>
        </div>
      </Card>
    )
  );
}
