import { Card, showToast } from "elbe-ui";
import { LucideImage, PartyPopper, Plus } from "lucide-react";
import { route } from "preact-router";
import { SBit } from "../../bit/sbit";
import { AppModel, AppService, ReadAppModel } from "../../service/s_app";
import { AuthBit } from "../../signal/b_auth";
import { showDateShort } from "../../util";
import { UserChip } from "../account/v_account_chip";
import { PlatformIcon } from "../v_platform_icons";

export function AppListOfUser({ id }: { id: string }) {
  const f = async () => AppService.i.list({ filter: `account = '${id}'` });

  return _AppList(f, false);
}

export function AppList(m: { sort: string; onHome?: boolean }) {
  const f = async () =>
    AppService.i.list({ sort: m.sort, filter: "status != 'archived'" });

  return _AppList(f, m.onHome || false);
}

function _AppList(f: () => Promise<ReadAppModel[]>, onHome: boolean) {
  const acc = AuthBit.use().map({ onData: (d) => d?.id });
  const { map } = SBit(f);

  return map.value({
    onError: (e) => <div>error</div>,
    onLoading: () => <div>loading...</div>,
    onData: (d) => (
      <div class="column cross-stretch">
        {d.length == 0 ? <div>no apps found</div> : null}

        {d.map((a) => AppSnippet({ app: a, account: acc }))}
      </div>
    ),
  });
}

export function AppInfo({ app }: { app: AppModel }) {
  return (
    <div class="row cross-start">
      {app.icon ? (
        <img
          src={app.icon}
          style="height: 4rem; width: 4rem; border-radius: 8px"
        />
      ) : (
        <div
          class="secondary rounded column cross-center main-center"
          style="width: 4rem; height:4rem;"
        >
          <LucideImage />
        </div>
      )}
      <div class="flex-1 column cross-stretch gap-half">
        <div class="b text-l">{app.name}</div>
        {app.expand.account ? <UserChip user={app.expand.account} /> : null}
      </div>
    </div>
  );
}

export function AppSnippet({
  app,
  account,
}: {
  app: AppModel;
  account: string;
}) {
  return (
    <Card
      class={
        "secondary row main-between cross-stretch pointer bordered " +
        (app.status == "draft" ? "secondary" : "")
      }
      style={{
        opacity: app.status == "archived" ? 0.5 : 1,
        cursor: app.status == "archived" ? "unset" : "pointer",
      }}
      onTap={() => {
        if (app.status == "archived") return;

        if (!account) {
          showToast("please log in to view app details");
          return;
        }
        route("/app/" + app.id);
      }}
    >
      <AppInfo app={app} />
      <div class="column cross-end main-space-between">
        <div class="row gap-half">
          {showDateShort(app.created)}{" "}
          <PlatformIcon platform={app.device_type} size={1.2} />
        </div>
        {app.status == "active" ? null : <div class="i b">{app.status}</div>}
      </div>
    </Card>
  );
}

function _oneOfFirst(signedIn: boolean) {
  return (
    <div class="card primary row gap-double">
      <PartyPopper class="action" style="height: 32px; width: 32px" />
      <div class="column cross-start action">
        <span>
          please help others by testing their apps and giving feedback
          <br />
          <b>publish your app</b>
        </span>
        <button
          class="loud minor"
          onClick={() => {
            if (!signedIn) {
              showToast("please log in to create an app");
              return;
            }
            route("/app/");
          }}
        >
          {" "}
          <Plus /> <div>create app</div>{" "}
        </button>
      </div>
    </div>
  );
}
