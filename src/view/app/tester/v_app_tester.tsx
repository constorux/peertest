import { ReadonlySignal } from "@preact/signals";
import { Button, Card } from "elbe-ui";
import { AlertTriangle } from "lucide-react";
import { AppModel } from "../../../service/s_app";
import { Info } from "../v_app_content";
import { AppTesterAndroid } from "./v_app_tester_android";
import { AppTesterWeb } from "./v_app_tester_web";

export function AppTesterView(p: {
  model: AppModel;
  update: (k: string, v: any) => void;
  editSignal: ReadonlySignal<boolean>;
}) {
  const m = p.model;
  return (
    <div class="column cross-stretch">
      <h3 class="margin-none" style="margin-top:2rem">
        test
      </h3>
      {!p.editSignal.value ? null : (
        <div class="column cross-stretch">
          <TesterToggle
            model={p.model}
            onToggle={(v) => p.update("public", v)}
          />
          <div class="action body-s i b">
            the following will only be shown to your testers
          </div>
        </div>
      )}
      {p.editSignal.value ? null : (
        <div class="body-s">give the dev feedback here:</div>
      )}
      <Info
        editing={p.editSignal.value}
        _id="email_feedback"
        label="Email address for feedback"
        classs="b"
        placeholder="feedback@example.com"
        multiline={false}
        value={p.model.email_feedback}
        onSubmit={p.update}
      />
      {p.model.device_type == "android" ? (
        <AppTesterAndroid {...p} />
      ) : (
        <AppTesterWeb {...p} />
      )}
    </div>
  );
}

function TesterToggle(m: { model: AppModel; onToggle: (v: boolean) => void }) {
  const publ = m.model.public;
  return (
    <div class="card padding-none" style="overflow: hidden">
      <div class="row">
        <Button
          manner={!publ ? "major" : "flat"}
          class="flex-1"
          onTap={() => m.onToggle(false)}
        >
          private
        </Button>
        <Button
          manner={publ ? "major" : "flat"}
          class="flex-1"
          onTap={() => m.onToggle(true)}
        >
          public
        </Button>
      </div>
      <div style="padding: 2rem 0">
        {publ ? (
          <div>
            every user can access your app.
            {m.model.device_type == "android" ? (
              <i>
                <br />
                <br />
                <b>For Google Play Closed Testing:</b> add the following google
                group to your list of private testers:
                <br />
                <b>peertest-org@googlegroups.com</b>
              </i>
            ) : null}
          </div>
        ) : (
          <div>
            only users you accept as testers will be able download your app.
            {m.model.device_type == "android" ? (
              <div class="column">
                <Card
                  manner="minor"
                  kind="warning"
                  class="row"
                  style={{ marginTop: "1rem" }}
                >
                  <AlertTriangle />
                  <div class="flex-1">
                    Your Android App will only be available to legacy testers.
                    It will not be available in the peerTest app.
                  </div>
                </Card>
                <i>
                  <b>For Google Play Closed Testing:</b> You'll have to add each
                  user's email address to the
                  <b> closed testers list</b> within the Google Play Console
                </i>{" "}
              </div>
            ) : null}
          </div>
        )}
      </div>
    </div>
  );
}
