import { ReadonlySignal } from "@preact/signals";
import { Button, Card } from "elbe-ui";
import { AlertTriangle } from "lucide-react";
import { AppModel } from "../../../service/s_app";
import { Info } from "../v_app_content";

export function AppTesterAndroid(p: {
  editSignal: ReadonlySignal<boolean>;
  model: AppModel;
  update: (k: string, v: any) => void;
}) {
  const model = p.model;
  return !p.editSignal.value ? (
    <div class="column cross-stretch">
      {p.model.public ? (
        <Button.flat
          onTap={() => {
            window.open("https://groups.google.com/g/peertest-org", "_blank");
          }}
        >
          1: join peerTest Google Group
        </Button.flat>
      ) : null}
      {p.model.url_download ? (
        <Button.major
          onTap={() => {
            window.open(p.model.url_download, "_blank");
          }}
        >
          download the app
        </Button.major>
      ) : null}
    </div>
  ) : (
    <div class="column cross-stretch">
      <i class="text-s">
        <br />
        <br />
        Make sure, your App is <b>available in all regions.</b> <br />
        Users will be able to download your app by clicking the link:
      </i>
      <Info
        editing={p.editSignal.value}
        _id="url_download"
        label="google play download link"
        classs="b"
        placeholder="https://play.google.com/..."
        multiline={false}
        value={model.url_download}
        onSubmit={p.update}
      />

      {!p.model.url_download?.startsWith(
        "https://play.google.com/apps/testing/"
      ) && (
        <Card manner="minor" kind="warning" class="row">
          <AlertTriangle />
          <div class="flex-1">
            Your link should start with:
            <div class="code">https://play.google.com/apps/testing/</div>
          </div>
        </Card>
      )}

      {p.model.url_download ? (
        <Button.minor
          class="action"
          onTap={() => {
            window.open(p.model.url_download, "_blank");
          }}
        >
          check your link
        </Button.minor>
      ) : null}
    </div>
  );
}
