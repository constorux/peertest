import { OwnMemberBit } from "../../../signal/b_is_tester";
import { AppModel, ReadAppModel } from "../../../service/s_app";
import { Info } from "../v_app_content";
import { ReadonlySignal } from "@preact/signals";

export function AppTesterWeb(p: {
  editSignal: ReadonlySignal<boolean>;
  model: AppModel;
  update: (k: string, v: any) => void;
}) {
  const model = p.model;
  return !p.editSignal.value ? (
    <div class="column cross-stretch">
      {p.model.url_download ? (
        <button
          class="loud minor"
          onClick={() => {
            window.open(p.model.url_download, "_blank");
          }}
        >
          open the app
        </button>
      ) : null}
    </div>
  ) : (
    <div class="column cross-stretch">
      <Info
        editing={p.editSignal.value}
        _id="url_download"
        label="link to your app"
        classs="b"
        placeholder="https://..."
        multiline={false}
        value={model.url_download}
        onSubmit={p.update}
      />

      {p.model.url_download ? (
        <button
          class="action"
          onClick={() => {
            window.open(p.model.url_download, "_blank");
          }}
        >
          check your link
        </button>
      ) : null}
    </div>
  );
}
