import { Button, Card, showConfirmDialog, Spaced, Text } from "elbe-ui";
import {
  FlaskConical,
  Info,
  LogOut,
  LucideMessagesSquare,
  Plus,
  Trash,
  User,
} from "lucide-react";
import { moewe } from "moewe";
import { SBit } from "../../bit/sbit";
import { ReadUserModel, UserService } from "../../service/s_user";
import { AuthBit } from "../../signal/b_auth";
import { UserBit } from "../../signal/b_user";
import { chooseImageFile, Field, go } from "../../util";
import { AppListOfUser } from "../app/v_app_list";
import { MemberOwnListView } from "../membership/v_member_own_list";
import { AppDownloadView } from "../v_ptapp";

function showDeleteDialog() {
  showConfirmDialog({
    title: "delete your account",
    message:
      "Hey there, sorry to see you go. I've not had the time to automate the deletion of records from the database. But that should not hinder you from having your data deleted.<br><br> So, just send me an email at <b class=\"b\">robin.naumann+peertest@proton.me</b> and I'll get in touch.",
    okay: true,
  }).then((ok) => {});
}

export function SparklesView({ data }: { data: ReadUserModel }) {
  function _Sparkle(data: ReadUserModel) {
    return (
      <div class="column cross-stretch">
        <Card
          onTap={() =>
            showConfirmDialog({
              title: "What are Beakers?",
              message: `For each you test, you get a Beaker. They replaced sparkles in this regard. Download the peerTest Android app to start testing. <br><br> You can upload your own app once you have 20 Beakers.`,
              okay: true,
            })
          }
          class="accent minor column cross-center"
        >
          <Text.h1 class="b centered">{data.expand.beaker.count}</Text.h1>
          <div class="row b gap-half">
            <FlaskConical /> Beakers
          </div>
        </Card>
        <div class="card secondary column padding-non cross-center gap-half">
          <div class="text">(legacy)</div>
          <div class="b centered">sparkles: {data.expand.sparkles.count}</div>
        </div>
      </div>
    );
  }
  if (data) return _Sparkle(data);

  const { map } = UserBit.use();
  return map({
    onError: (e) => <div>error</div>,
    onLoading: () => <div>loading</div>,
    onData: (d) => _Sparkle(d),
  });
}

function _AccountField({
  fId,
  placeholder,
  multiline = false,
}: {
  fId: string;
  placeholder: string;
  multiline?: boolean;
}) {
  const { map, ctrl } = UserBit.use();
  return map({
    onData: (d) => (
      <Field
        value={d[fId]}
        label={placeholder}
        multiline={multiline}
        onSubmit={(v) => ctrl.set({ [fId]: v })}
      />
    ),
  });
}

function UserView({ data }: { data?: ReadUserModel }) {
  if (data)
    return (
      <div class="column cross-stretch">
        <div class="b text-l">{data.name}</div>
        <div>{data.description}</div>
        {data.homepage ? (
          <a target="_blank" href={data.homepage}>
            {data.homepage}{" "}
          </a>
        ) : null}
      </div>
    );
}

function OwnUserView({}) {
  const { map, ctrl } = UserBit.use();

  return map({
    onData: (d) => (
      <div class="column cross-stretch">
        <h3 style="margin: 0">Your Account</h3>
        <div class="b">{d.email}</div>
        <div class="card secondary row">
          <Info style="width: 32px; height: 32px" />
          <div class="text-s">
            Info: I've not found the time to implement email notifications. So
            check this page (and your apps) regularly for new testers
          </div>
        </div>
        The information below is shown to other users
        <Spaced amount={0.2} />
        <_AccountField fId="name" placeholder="your name" />
        <_AccountField
          fId="description"
          placeholder="about you"
          multiline={true}
        />
        <_AccountField fId="homepage" placeholder="your homepage" />
      </div>
    ),
  });
}

export function AccountView({ account_id }: { account_id: string }) {
  const f = !account_id ? () => null : () => UserService.i.get(account_id);

  const { map } = SBit<ReadUserModel | null>(f);
  const { map: authMap, ctrl: authCtrl } = AuthBit.use();

  return map.value({
    onError: (_) => <div>error</div>,
    onLoading: () => <div>loading...</div>,
    onData: (d) => (
      <div class="base-limited column cross-stretch">
        <Spaced amount={2} />
        <ProfileImage data={d} />
        <Spaced amount={1} />

        <div class="row-resp gap-double">
          <div class="column cross-stretch flex-3">
            {d ? <UserView data={d} /> : <OwnUserView />}
            <h3 style="margin-bottom: 0"> Your Apps</h3>
            <AppListOfUser id={d?.id || authMap({ onData: (d) => d.id })} />

            {<h3 style="margin-bottom: 0">Tests</h3>}
            <AppDownloadView />
            {d ? null : <h3 style="margin-bottom: 0">Legacy Tests</h3>}
            {d ? null : <MemberOwnListView />}
          </div>
          <div class="column flex-1 cross-stretch">
            <SparklesView data={d} />
            {d ? null : <_CreateAppButton />}

            {authMap({
              onData: (auth) =>
                auth && !d ? (
                  <div class="column cross-stretch">
                    <Button.plain
                      onTap={() =>
                        moewe().ui.showFeedbackDialog({
                          labels: {
                            title: "peertest.org Feedback",
                          },
                        })
                      }
                      class="action"
                    >
                      <LucideMessagesSquare />
                      feedback
                    </Button.plain>
                    <Button.plain onTap={authCtrl.logout} class="action">
                      <LogOut />
                      log out
                    </Button.plain>

                    <Button.plain
                      kind="error"
                      onTap={showDeleteDialog}
                      class="integrated"
                    >
                      <Trash />
                      delete
                    </Button.plain>
                  </div>
                ) : null,
            })}
          </div>
        </div>
      </div>
    ),
  });
}

function _CreateAppButton() {
  const { map } = UserBit.use();

  return map({
    onError: (e) => <div>error</div>,
    onLoading: () => <div>loading</div>,
    onData: (d) => {
      const sufficient =
        Math.max(d.expand.beaker.count, d.expand.sparkles.count) >= 20;
      return (
        <Button.major
          onTap={sufficient ? go("app") : null}
          tooltip={sufficient ? "" : "You need 20 Beakers to create an app"}
        >
          <Plus /> <div>create app</div>
        </Button.major>
      );
    },
  });
}

function ProfileImage({ data }) {
  if (data) {
    return (
      <div class="column">
        {data.avatar ? (
          <img src={data.avatar} class="round secondary profile-img " />
        ) : (
          <div class="secondary round profile-img column cross-center main-center ">
            <User />
          </div>
        )}
      </div>
    );
  }

  const { map, ctrl } = UserBit.use();

  async function setImg() {
    const file = await chooseImageFile();
    if (!file) return;

    const fd: FormData = new FormData();
    fd.append("avatar", file);
    ctrl.set(fd);
  }

  return map({
    onData: (d) => (
      <div class="column">
        {d.avatar ? (
          <img
            src={d.avatar}
            class="round secondary profile-img pointer"
            onClick={setImg}
          />
        ) : (
          <div
            class="secondary round profile-img column main-center cross-center pointer"
            onClick={setImg}
          >
            <User />
          </div>
        )}
        <div class="text-s i">max. 100 kB</div>
      </div>
    ),
  });
}
