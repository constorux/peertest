import { Box } from "elbe-ui";
import { FlaskConical, User } from "lucide-react";
import { UserModel } from "../../service/s_user";
import { go } from "../../util";

export function UserChip({ user }: { user: UserModel }) {
  if (!user) return null;
  return (
    <div
      class="row main-start gap-half pointer"
      onClick={go("/account/" + user.id)}
    >
      <_UserImg img={user.avatar} />
      <div class="">{user.name || "unnamed user"}</div>
      {user.expand.sparkles ? (
        <Box
          scheme="primary"
          class="b row gap-none cross-center"
          style={{ background: "transparent" }}
          tooltip="this reflects the number of apps a user has tested"
        >
          <FlaskConical height="0.9rem" />
          {user.expand.beaker.count}
        </Box>
      ) : null}
    </div>
  );
}

function _UserImg({ img }: { img: string }) {
  return !img ? (
    <div
      class="secondary round column cross-center main-center"
      style="height:1.5rem; width:1.5rem"
    >
      <User style="width: 1rem" />
    </div>
  ) : (
    <img
      class="secondary round column cross-center main-center"
      style="height:1.5rem; width:1.5rem"
      src={img}
    ></img>
  );
}
