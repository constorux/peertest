import { useSignal } from "@preact/signals";
import { Card, Text } from "elbe-ui";
import { ChevronDown, ChevronUp, SortDesc, Timer } from "lucide-react";
import { moewe } from "moewe";
import { AppList } from "./app/v_app_list";
import { AppUpdateView } from "./v_ptapp";

function _HomeMessage() {
  const show = useSignal(false);
  return (
    <div class="card secondary column cross-stretch">
      <div
        class="row pointer"
        onClick={() => {
          show.value = !show.value;
        }}
      >
        <div class="flex-1 b">about this page</div>
        <button class="action">
          {show.value ? <ChevronUp /> : <ChevronDown />}
        </button>
      </div>
      <i>find fellow testers for your development project</i>

      <b>
        if you find a bug/have a question{" "}
        <a
          class="pointer"
          onClick={() =>
            moewe().ui.showFeedbackDialog({
              labels: {
                title: "peertest.org Feedback",
              },
            })
          }
        >
          just let me know
        </a>{" "}
        :)
      </b>
      {!show.value ? null : (
        <div>
          <b>Hi there.</b> Since the end of last year, Google requires new
          developers to test their apps with at least 20 users before publishing
          them on the Play Store. To make it easier for developers to find
          testers, I spent a couple weekends building this website.
          <br /> Here, developers can test other peoples apps and earn credits
          called sparkles. You can then spend 20 of these to add your own app.
          <br />
          <br />
          <b>
            Of course you can also just use this site to find testers for your
            already published app or website.
          </b>
          <br />
          <br />I hope you find it useful :)
          <div style="text-align: end; font-weight: bold; margin-top: 3rem; margin-bottom: 2rem">
            Yours, Robin{" "}
          </div>
          <span class="text-s">
            This is <b>FOSS</b> build with{" "}
            <a class="b" href="https://preactjs.com/">
              Preact
            </a>{" "}
            and{" "}
            <a class="b" href="https://pocketbase.io/">
              PocketBase
            </a>{" "}
          </span>
        </div>
      )}
    </div>
  );
}

export function HomeView({}) {
  const sortSignal = useSignal(true);

  return (
    <div class="base-limited column cross-stretch gap-double">
      <_HomeMessage />
      <AppUpdateView />
      <Text.h3 v="available apps" />

      <div class="row-resp resp-reverse gap-double">
        <div class="column cross-stretch main-start flex-3">
          <AppList
            sort={sortSignal.value ? "-created" : "+name"}
            onHome={true}
          />
        </div>
        <div class="column flex-1 cross-stretch">
          <Card
            kind="accent"
            manner="minor"
            class="column cross-stretch gap-none"
            style={{
              padding: "0px",
              background: "transparent",
              overflow: "hidden",
            }}
          >
            <button
              class={
                "margin-none sharp text-left " +
                (sortSignal.value ? "loud minor" : "integrated")
              }
              onClick={() => {
                sortSignal.value = true;
              }}
            >
              <Timer /> <div>recent</div>
            </button>
            <button
              class={
                "margin-none sharp text-left " +
                (!sortSignal.value ? "loud minor" : "integrated")
              }
              onClick={() => (sortSignal.value = false)}
            >
              <SortDesc /> <div>sorted</div>
            </button>
          </Card>
        </div>
      </div>
      <RedditLink />
    </div>
  );
}

function RedditLink(m: {}) {
  return (
    <div class="row main-center cross-start" style="margin-top: 3rem">
      <b style="margin-top: 0.45rem">definitly also check out</b>
      <div class="column main-start gap-half cross-start">
        <a
          href="https://flutter-launchpad.web.app/"
          class="sister-link"
          style="background-color: #6262f0;"
          target="_blank"
        >
          🚀 Flutter Launchpad
        </a>
        <a
          href="https://www.reddit.com/r/AndroidClosedTesting/"
          class="sister-link"
          style="background-color: #ff4500;"
          target="_blank"
        >
          r/AndroidClosedTesting
        </a>
      </div>
    </div>
  );
}
