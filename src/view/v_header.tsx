import { Button, Text } from "elbe-ui";
import { User } from "lucide-react";
import { route } from "preact-router";
import { Spinner } from "..";
import { AuthBit } from "../signal/b_auth";
import { asString } from "../util";
import { LoginWithGoogleView } from "./v_login";

export function HeaderView() {
  return (
    <div>
      <div style="height: 5rem"></div>
      <div class="header">
        <div class="header-title flex-1 cross-start" onClick={goHome}>
          <div style="font-weight: normal">
            <b class="b">peerTest</b>
            <span class="accent flat b">•org</span>
          </div>
        </div>

        <ProfileButton />
      </div>
    </div>
  );
}

function goAccount() {
  route("/account");
}

function goHome() {
  route("/");
}

function ProfileButton() {
  const { map } = AuthBit.use();
  return map({
    onData: (d) =>
      !d ? (
        <LoginWithGoogleView />
      ) : (
        <Button.plain onTap={goAccount}>
          <Text class="if-wide" style={{ fontSize: "1rem" }} v={d?.email} />
          <User />
        </Button.plain>
      ),
    onLoading: () => <Spinner />,
    onError: (e) => <div>{asString(e)}</div>,
  });
}
