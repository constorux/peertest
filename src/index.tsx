import { ElbeTheme, LayerColor } from "elbe-ui";
import "elbe-ui/dist/elbe.css";
import { Loader2 } from "lucide-react";
import { moewe, Moewe } from "moewe";
import { Component, render } from "preact";
import { Route, route, Router } from "preact-router";
import { useEffect } from "preact/hooks";
import "../style/base.scss";
import "../style/google.scss";
import { AuthService, AuthState } from "./service/s_auth";
import { AuthBit } from "./signal/b_auth";
import { UserBit } from "./signal/b_user";
import { AccountView } from "./view/account/v_account";
import { AppContentView } from "./view/app/v_app_content";
import { FooterView } from "./view/v_footer";
import { HeaderView } from "./view/v_header";
import { HomeView } from "./view/v_home";

export const appVersion = "0.8.17";

function _Router({}) {
  return (
    <Router>
      <Route path="/" component={HomeView} />
      <ProtectedRoute path="/app/:app_id?" component={AppContentView} />
      <ProtectedRoute path="/account/:account_id?" component={AccountView} />
    </Router>
  );
}

class App extends Component {
  async componentDidMount() {
    // Check if user is authenticated here
    // You can call the guard method here
    this.guard(window.location.pathname, await AuthService.i.get());
  }

  // some method that returns a promise
  guard(url: string, user: AuthState) {
    if (user) return;
    route("/", true);
  }

  render() {
    const { map } = AuthBit.use();

    return map({
      onData: (d) => (
        <div>
          <HeaderView />
          {d ? (
            <UserBit.Provide id={d.id}>
              {" "}
              <_Router />{" "}
            </UserBit.Provide>
          ) : (
            <_Router />
          )}
          <FooterView />
          <div style="height:0px width: 0px; border: solid 1px transparent"></div>
        </div>
      ),

      onError: (e) => <span>error</span>,
      onLoading: () => <Spinner />,
    });
  }
}

function Root() {
  return (
    <div class="content-base elbe-base primary">
      <ElbeTheme
        seed={{
          color: {
            accent: LayerColor.fromHex("#6d38aa"),
          },
          type: {
            heading: {
              bold: true,
              family: ["Space Grotesk", "monospace"],
              size: 2,
            },
            body: {
              family: ["Space Grotesk", "monospace"],
              size: 1,
            },
          },
        }}
        dark={false}
      >
        <AuthBit.Provide>
          <App />
        </AuthBit.Provide>
      </ElbeTheme>
    </div>
  );
}

export function Spinner() {
  return (
    <div style="margin: 5rem 0" class="centered padded">
      {" "}
      <div class="rotate-box">
        <Loader2 />
      </div>
    </div>
  );
}

function ProtectedRoute(props: any) {
  const { map } = AuthBit.use();

  const isLoggedIn = map({
    onData: (d) => !!d,
    onError: (e) => false,
    onLoading: () => false,
  });

  useEffect(() => {
    if (!isLoggedIn) {
      route("/", true);
    }
  }, [isLoggedIn]);

  if (!isLoggedIn) return null;
  return <Route {...props} />;
}

async function main() {
  await new Moewe({
    host: "open.moewe.app",
    project: "a9c24f15a69ba10d",
    app: "04d9a619a614850c",
  }).init();

  moewe().events.appOpen({});

  render(<Root />, document.getElementById("app"));
}
main();
